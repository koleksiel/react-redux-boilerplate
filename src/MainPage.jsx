import React from "react";
import { Route, BrowserRouter as Router } from "react-router-dom";

import Menu from "./Menu";
import Welcome from "./Welcome/Welcome";
import Dokumentasi from "./Dokumentasi/Dokumentasi";
import DetailContent from "./SharedComponents/DetailContent";

const MainPage = () => (
  <Router>
    <div className="container-fluid">
      <div className="row navigation">
        <div className="col-md-12">
          <Menu />
        </div>
      </div>
      <div className="row">
        <Route exact path="/" component={Welcome} />
        <Route path="/welcome" component={Welcome} />
        <Route path="/dokumentasi" component={Dokumentasi} />
        <Route path="/detail" component={DetailContent} />
      </div>
    </div>
  </Router>
);

export default MainPage;
