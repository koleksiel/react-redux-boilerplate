import React from "react";
import Faker from "faker";
import ContentItem from "../SharedComponents/ContentItem";
import FeatureItem from "../SharedComponents/FeatureItem";
import DataTable from "../SharedComponents/DataTable";

import contentImg from "../../assets/images/react.png";

const countItem = [1, 2, 3];

const arrItems = [
  {
    coverImg: Faker.image.technics(),
    url: "/detail",
    title: "React Redux Tutorial Part 1",
    author: Faker.name.firstName(),
    date: "22 April 2018",
    contentBody:
      "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum"
  }
];

const Dokumentasi = () => (
  <div className="col-md-12">
    <div className="container">
      <div className="row">
        {countItem.map(() => (
          <div className="col-md-4">
            <FeatureItem data={{ featureImg: Faker.image.business() }} />
          </div>
        ))}
      </div>

      <div className="row">
        <div className="col-md-12">
          {arrItems.map(item => (
            <ContentItem data={item} />
          ))}
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <DataTable />
        </div>
      </div>
    </div>
  </div>
);

export default Dokumentasi;
