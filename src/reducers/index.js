import { combineReducers } from "redux";

import { welcomeReducer } from "./welcomeReducer";

export default combineReducers({
  welcome_store: welcomeReducer
});
