/* eslint default-case: "off" */
export function welcomeReducer(state = { welcome: [] }, action) {
  switch (action.type) {
    case "WELCOME":
      return { welcome: [...state.welcome, ...action.payload] };
      break;
  }
  return state;
}
