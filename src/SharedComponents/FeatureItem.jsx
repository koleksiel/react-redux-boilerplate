import React from "react";
import PropTypes from "prop-types";

const FeatureItem = props => (
  <div className="row">
    <div className="feature-container">
      <div className="col-md-12 image-container">
        <img src={props.data.featureImg} width="100%" alt="" />
      </div>
      <div className="col-md-12">
        <p>
          Lorem ipsum dolor ipsum Dolor Ipsum Lorem ipsum dolor ipsum Dolor
          Ipsum Lorem ipsum dolor ipsum Dolor Ipsum
        </p>
      </div>
    </div>
  </div>
);

FeatureItem.defaultProps = {
  data: {}
};

FeatureItem.propTypes = {
  data: PropTypes.objectOf(PropTypes.object)
};

export default FeatureItem;
