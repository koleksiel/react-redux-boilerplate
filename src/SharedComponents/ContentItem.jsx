import React from "react";
import PropTypes from "prop-types";

const ContentItem = props => (
  <div className="news-container">
    <div className="row text-center">
      <div className="col-sm-12 col-md-4 cover-img">
        <img src={props.data.coverImg} width="100%" alt="" />
      </div>
      <div className="col-sm-12 col-md-8 news-content">
        <div className="content-header">
          <h2>
            <a href={props.data.url}>{props.data.title}</a>
          </h2>
          <p className="content-author">
            Author : {props.data.author} | {props.data.date}
          </p>
        </div>
        <div className="content-body">
          <p>{props.data.contentBody}</p>
        </div>
      </div>
    </div>
  </div>
);

ContentItem.defaultProps = {
  data: {}
};

ContentItem.propTypes = {
  data: PropTypes.objectOf(PropTypes.object)
};
export default ContentItem;
