import React from "react";
import Faker from "faker";

const DetailContent = props => (
  <div className="col-md-12">
    <div className="detail-container">
      <div className="detail-content-header">
        <h2>React Redux Tutorial Part 1</h2>
        <p>Author : {Faker.name.firstName()} | 22 April 2018</p>
      </div>
      <div className="detail-content-body">
        <p>
          Lorem Ipsum Dolor Ipsum LOrelm Lorem Ipsum Dolor Ipsum LOrelm Lorem
          Ipsum Dolor Ipsum LOrelm Lorem Ipsum Dolor Ipsum LOrelm Lorem Ipsum
          Dolor Ipsum LOrelm Lorem Ipsum Dolor Ipsum LOrelm Lorem Ipsum Dolor
          Ipsum LOrelm Lorem Ipsum Dolor Ipsum LOrelm Lorem Ipsum Dolor Ipsum
          LOrelm Lorem Ipsum Dolor Ipsum LOrelm v vLorem Ipsum Dolor Ipsum
          LOrelm Lorem Ipsum Dolor Ipsum LOrelm Lorem Ipsum Dolor Ipsum LOrelm
          Lorem Ipsum Dolor Ipsum LOrelm Lorem Ipsum Dolor Ipsum LOrelm v vLorem
          Ipsum Dolor Ipsum LOrel mLorem Ipsum Dolor Ipsum LOrelm Lorem Ipsum
          Dolor Ipsum Ipsum Ipsum Ipsum Ipsum Ipsum Ipsum LOrelmLorem Ipsum
          Dolor Ipsum LOrelm Ipsum Dolor Ipsum LOrelm Lorem Ipsum Dolor Ipsum
          LOrelm
        </p>
      </div>
    </div>
  </div>
);

export default DetailContent;
