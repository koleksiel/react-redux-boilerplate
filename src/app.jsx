// React import
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

// Redux import
import { applyMiddleware, createStore } from "redux";
import logger from "redux-logger";
import reducers from "./reducers/index";

// import component utama
import MainPage from "./MainPage";

// import base scss styles
import base from "../assets/scss/base.scss";

// untuk melihat state redux, memudahkan dalam proses development
// untuk menambahkah middleware, import middleware dan tambahkan pada applyMiddleware.
// contoh : "applyMiddleware(logger, thunk)"
const middleware = applyMiddleware(logger);
const store = createStore(reducers, middleware);

ReactDOM.render(
  <Provider store={store}>
    <MainPage />
  </Provider>,
  document.getElementById("app")
);
