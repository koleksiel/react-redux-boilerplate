/*
  File utama digunakan untuk mengolah data / perhitungan yang diperlukan.
  pada file ini anda dapat menangkap data dari redux store
  mengolah data tersebut kemudian mengirim data kepada sub komponen melalui parameter
  pada sub komponen
*/
// import react, prop-types
import React from "react";
import PropTypes from "prop-types";

// import redux and actions
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { welcomeAction } from "./actions/sampleAction";

// import components
import Logo from "./components/Logo";
import Greetings from "./components/Greetings";
import Included from "./components/Included";

class Welcome extends React.Component {
  componentDidMount() {
    this.props.welcomeAction({
      id: 101,
      text:
        "Instalasi berhasil, langkah berikutnya anda dapat membuat component baru atau fungsi baru sesuai kebutuhan anda."
    });
  }

  render() {
    const arrUcapan = this.props.welcome_state.map(data => data.text);

    const ucapan = arrUcapan[0];

    const includeItems = [
      { package: "React", version: "^16.2.0" },
      { package: "Redux", version: "^3.7.2" },
      { package: "Webpack", version: "^3.11.0" },
      { package: "jQuery", version: "^1.9.1" },
      { package: "Bootstrap", version: "^4.0.0" },
      { package: "SASS", version: "^4.7.2" }
    ];

    return (
      <div className="col-md-12">
        <div className="row">
          {/* logo */}
          <div className="col-md-12">
            <Logo styles="logo" logo="BOIRE" />
          </div>
        </div>
        <div className="row">
          {/* greetings */}
          <div className="col-md-12">
            <div className="selamat">
              <Greetings judul="Selamat !!" ucapan={ucapan} />
            </div>
          </div>
        </div>
        <div className="row">
          {/* included */}
          <div className="col-md-12">
            <Included judul="Included :" items={includeItems} />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    welcome_state: state.welcome_store.welcome
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      welcomeAction
    },
    dispatch
  );
}

Welcome.defaultProps = {
  welcomeAction: "",
  welcome_state: []
};

Welcome.propTypes = {
  welcomeAction: PropTypes.func,
  welcome_state: PropTypes.arrayOf(PropTypes.object)
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Welcome);
