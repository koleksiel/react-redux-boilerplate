/*
  sub komponen ini digunakan hanya untuk menampilkan hasil
  jika terdapat proses perhitungan atau pengolahan lainnya,
  maka proses tersebut dilakukan pada komponen utama.
  data yang telah diolah dapat diperoleh melalui props
*/

import React from "react";
import PropTypes from "prop-types";

const Included = ({ judul, items }) => (
  <div className="included">
    <h4 className="h4">{judul}</h4>
    <ul>
      {items.map((data, index) => (
        <li key={data.package}>
          {`${data.package} `}
          <span className="badge badge-pill badge-info">{data.version}</span>
        </li>
      ))}
    </ul>
  </div>
);

Included.defaultProps = {
  judul: "",
  items: []
};

Included.propTypes = {
  judul: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.object)
};

export default Included;
