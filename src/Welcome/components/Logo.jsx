/*
  sub komponen ini digunakan hanya untuk menampilkan hasil
  jika terdapat proses perhitungan atau pengolahan lainnya,
  maka proses tersebut dilakukan pada komponen utama.
  data yang telah diolah dapat diperoleh melalui props
*/

import React from "react";
import PropTypes from "prop-types";

const Logo = ({ styles, logo }) => <div className={styles}>{logo}</div>;

Logo.defaultProps = {
  styles: "",
  logo: "BOIRE"
};

Logo.propTypes = {
  styles: PropTypes.string,
  logo: PropTypes.string
};

export default Logo;
