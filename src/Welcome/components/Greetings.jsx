/* eslint arrow-body-style: ["error", "always"] */
/*
  sub komponen ini digunakan hanya untuk menampilkan hasil
  jika terdapat proses perhitungan atau pengolahan lainnya,
  maka proses tersebut dilakukan pada komponen utama.
  data yang telah diolah dapat diperoleh melalui props
*/

import React from "react";
import PropTypes from "prop-types";

const Greetings = ({ judul, ucapan }) => {
  return (
    <div>
      <span className="title">{judul}</span>
      <p>{ucapan}</p>
    </div>
  );
};

Greetings.defaultProps = {
  judul: "",
  ucapan: ""
};

Greetings.propTypes = {
  judul: PropTypes.string,
  ucapan: PropTypes.string
};

export default Greetings;
