const path = require("path");
const express = require("express");

const app = express();

app.use(express.static("public"));
app.use(express.static("dist"));

app.get("*", (req, res) =>
  res.sendFile(path.resolve(__dirname, "public", "index.html"))
);

app.listen(1900, () =>
  console.log("BoiRe Listening on : http://localhost:1900")
);
